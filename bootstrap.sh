#!/bin/sh
set -x
cd $HOME
git clone https://gitlab.com/rcy/dotfiles.git
make -C dotfiles/ reset dotfiles
