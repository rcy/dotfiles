;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

;; https://github.com/raxod502/straight.el
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;;; start emacs server
(ignore-errors (server-start))

(require 'package)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)

(load "~/elisp/rcy-util.el")

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(comint-buffer-maximum-size 9999)
 '(display-time-use-mail-icon t)
 '(grep-find-ignored-directories
   (quote
    ("SCCS" "RCS" "CVS" "MCVS" ".src" ".svn" ".git" ".hg" ".bzr" "_MTN" "_darcs" "{arch}" ".meteor" "node_modules" "build")))
 '(indent-tabs-mode nil)
 '(inhibit-startup-screen t)
 '(js-indent-level 2)
 '(notmuch-fcc-dirs "fastmail/sent")
 '(notmuch-saved-searches
   (quote
    ((:name "inbox" :query "tag:inbox" :key "i")
     (:name "flagged" :query "tag:flagged" :key "f")
     (:name "sent" :query "tag:sent" :key "t")
     (:name "drafts" :query "tag:draft" :key "d")
     (:name "all" :query "*" :key "a" :sort-order newest-first)
     (:name "later" :query "tag:later")
     (:name "unread" :query "tag:unread")
     (:name "todo" :query "tag:todo"))))
 '(notmuch-search-oldest-first nil)
 '(org-archive-location ".archive.org::datetree/* From %s")
 '(org-babel-load-languages (quote ((emacs-lisp . t) (js . t))))
 '(org-confirm-babel-evaluate nil)
 '(org-cycle-global-at-bob t)
 '(org-cycle-hook
   (quote
    (org-inlinetask-hide-tasks org-cycle-hide-archived-subtrees org-cycle-show-empty-lines)))
 '(org-stuck-projects
   (quote
    ("+LEVEL=2/-DONE"
     ("NEXT" "DELEGATED" "WAITFOR")
     nil "")))
 '(org-use-speed-commands t)
 '(package-selected-packages
   (quote
    (forge graphql-mode toml-mode rust-mode ob-mongo dockerfile-mode less-css-mode hackernews graphviz-dot-mode go-mode markdown-mode yaml-mode helm git-link)))
 '(pcomplete-ignore-case t)
 '(remember-data-file "~/gtd/inbox.org")
 '(send-mail-function (quote smtpmail-send-it))
 '(smtpmail-smtp-server "smtp.fastmail.com")
 '(smtpmail-smtp-service 587)
 '(tab-width 8)
 '(vc-follow-symlinks t)
 '(web-mode-code-indent-offset 2)
 '(web-mode-enable-auto-quoting nil)
 '(web-mode-enable-comment-interpolation t)
 '(web-mode-enable-engine-detection t)
 '(web-mode-markup-indent-offset 2))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;;; theme
(menu-bar-mode -1)
(when window-system
  (set-background-color "black")
  (set-foreground-color "gray90")
  (set-default-font ":pixelsize=30")
  (scroll-bar-mode -1)
  (tool-bar-mode -1))
(display-time-mode 1)
(display-battery-mode)

;; convenience
(show-paren-mode 1)

;;; keybindings
(ffap-bindings)
(global-set-key (kbd "C-c C") 'compile)
(global-set-key (kbd "C-c P") 'rcy-insert-xkcd-password)
(global-set-key (kbd "C-c R") 'remember)
(global-set-key (kbd "C-c T") 'rcy-insert-time)
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c g") 'git-link)
(global-set-key (kbd "C-c h") 'hl-line-mode)
(global-set-key (kbd "C-c i") 'rcy-insert-random-id)
(global-set-key (kbd "C-c j") 'rcy-prettify-json-region)
(global-set-key (kbd "C-c k") 'comment-region)
(global-set-key (kbd "C-c m") 'notmuch)
(global-set-key (kbd "C-c r") 'org-capture)
(global-set-key (kbd "C-c q") 'quick-calc)
(global-set-key (kbd "C-x g") 'magit-status)

;;; dired
(require 'dired-x)

;;; web-mode
(straight-use-package 'web-mode)
(setq web-mode-content-types-alist
      '(("jsx" . "\\.js[x]?\\'")
        ("javascript" . "\\.mjs\\'")))

(add-to-list 'auto-mode-alist '("\\.mjs\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.jsx?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html\\'" . web-mode))
(add-to-list 'auto-mode-alist '("Gemfile." . ruby-mode))

(setq web-mode-comment-formats '(("javascript" . "//")
                                 ("jsx" . "//")))
(setq web-mode-engines-alist '(("meteor" . "\\.html\\'")))

;;; ido
(ido-mode 1)

;;; comint
(add-hook 'comint-output-filter-functions 'comint-truncate-buffer)

;;; browser
(setq browse-url-browser-function 'browse-url-chrome)

;;; projectile
(straight-use-package 'projectile)
(when (fboundp 'projectile-mode)
  (projectile-mode 1)
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map))

;;; enable
(put 'scroll-left 'disabled nil)

;;; org
(setq org-agenda-files '("~/gtd/projects.org" "~/gtd/actions.org"))

(setq org-capture-templates '(("i" "Inbox" entry
                               (file+headline "~/gtd/inbox.org" "Inbox")
                               "* %i%?")
                              ("a" "Action" entry
                               (file+headline "~/gtd/actions.org" "Inbox")
                               "* NEXT %i%?")))

(setq org-refile-targets
      '(
        ("~/gtd/projects.org" :maxlevel . 2)
        ("~/gtd/actions.org" :maxlevel . 2)
        ("~/gtd/someday.org" :maxlevel . 2)
        ("~/gtd/magic.org" :maxlevel . 2)
        ("~/gtd/lists.org" :maxlevel . 2)
        ("~/gtd/diary.org" :maxlevel . 2)
        ("~/gtd/agendas.org" :maxlevel . 3)
        ("~/gtd/reference.org" :maxlevel . 2)
        ))

(setq org-todo-keywords '((sequence "NEXT(n)" "WAITFOR(w)" "DELEGATED(g)" "|" "DONE(d)" "CANCELLED(c)")))

(setq org-agenda-text-search-extra-files
      (mapcar (lambda (x) (concat "~/gtd/" x ".org"))
              '("goals" "magic" "someday" "lists" "diary" ".archive")))

(setq org-agenda-custom-commands
      '(("n" "Next actions" ((todo "NEXT")))
        ("r" "Review" ((agenda "") (alltodo "") (stuck "")))
        ("N" "Agenda and all TODOs" ((agenda "") (alltodo "")))
        ("x" "Agenda for priority A" todo ""
         ((org-agenda-skip-function
           '(and
             (not
              (equal "A"
                     (org-entry-get nil "PRIORITY")))
             (point-at-eol)))))
        ("p" "Phone Calls" ((tags "+phone")))
        ))
(require 'org-inlinetask)
(require 'ob-shell)
(setq dired-guess-shell-alist-user
      '(("\\.pdf$" "evince")))
(straight-use-package 'neotree)
(global-set-key (kbd "C-c n") 'neotree-toggle)

(straight-use-package 'magit)
